'use-strict'

// Compile dep
import 'babel-polyfill'

// Third party styles
import 'bootstrap/dist/css/bootstrap.css';

// Third party dependencies
import angular from 'angular'

// Module dependencies
import root from './root'
import dashboard from './dashboard'

angular.module('brandbox', [
    root,
    dashboard
])
