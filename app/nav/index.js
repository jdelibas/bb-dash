'use strict'

// Third party deps
import angular from 'angular'
import uiRouter from 'angular-ui-router'

// Module styles
import './top-nav.styles.css'
import './left-nav.styles.css'

// Module components

export default angular.module('brandbox.nav', [])
  .name