import dashboardView from './dashboard.view.html';

config.$inject = ['$stateProvider'];
export default function config($stateProvider) {
    $stateProvider
    .state('root.dashboard', {
        url: '/dashboard',
        views: {
            'content@': {
                template: dashboardView
            }
        }
    })
}