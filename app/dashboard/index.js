'use strict'

// Third party deps
import angular from 'angular'
import uiRouter from 'angular-ui-router'

// Module components
import config from './dashboard.config'

export default angular.module('brandbox.dashboard', [
    uiRouter
])
  .config(config)
  .name