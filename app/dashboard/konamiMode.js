'use strict';

module.exports = function(ngModule) {
    /**
     * @module      ngModule.iweKonamiMode
     * @description creates a dev mode state, must add ?dev into state
     */
    ngModule.directive('iweKonamiMode', konamiModeDirective);

    konamiModeDirective.$inject = ['$rootScope'];
    function konamiModeDirective($rootScope) {

        function linkFn(scope, element) {
            var keys = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
            var matchCount = 0;
            element.on('keydown', onKeydown);

            function onKeydown(event) {
                if (event.which === keys[matchCount++]) {
                    if (matchCount === keys.length) {
                        console.log('konami');
                        $rootScope.$broadcast('KONAMI_MODE', event.target);
                        element.off('keydown', onKeydown);
                    }
                    return;
                }
                matchCount = 0;
            }
        }

        return {
            restrict: 'A',
            link: linkFn
        };
    }

};