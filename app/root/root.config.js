'use strict'

import topNavView from './../nav/top-nav.view.html'
import leftNavView from './../nav/left-nav.view.html'

config.$inject = ['$stateProvider']
export default function config($stateProvider) {
  $stateProvider
    .state('root', {
      url: '',
      views: {
        'top-nav@': {
          template: topNavView
        },
        'left-nav@': {
          template: leftNavView
        }
      }
    })
}
