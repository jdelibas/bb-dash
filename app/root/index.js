'use strict'

// Third party deps
import angular from 'angular'
import uiRouter from 'angular-ui-router'

// Module dependencies
import nav from './../nav'

// Module components
import config from './root.config'

export default angular.module('brandbox.core', [
  uiRouter
])
  .config(config)
  .name