'use strict'

const webpack = require('webpack');

let config = {
  entry: './app/index.js',
  output: {
    path: './app',
    filename: 'app.bundle.js',
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader?-minimize'
    }, {
      test: /\.html$/,
      loader: 'html-loader'
    },{
      test: /\.(woff|woff2)$/,
      loader: 'file?name=fonts/[name].[ext]'
    },
    {
      test: /\.(eot|svg|ttf)$/,
      loader: 'file?name=fonts/[name].[ext]'
    }]
  },
  plugins: []
}

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false
      }
    })
  )
}

module.exports = config